var express = require('express'),
app = express(),
port = process.env.PORT || 3000,
mongoose = require('mongoose'),
Deck = require('./api/models/pokemonModels'),
bodyParser = require('body-parser');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect(process.env.PROD_MONGODB,{useMongoClient: true}); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/pokemonRoutes');
routes(app);


app.listen(port);


console.log('MyPokemon RESTful API server started on: ' + port);
