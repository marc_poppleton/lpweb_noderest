'use strict';
module.exports = function(app) {
  var pokemons = require('../controllers/pokemonController');

  app.route('/decks')
  .get(pokemons.load_all_decks)
  .post(pokemons.create_a_deck_for_user);

  app.route(':userId/decks')
    .get(pokemons.load_all_decks_for_user)

  app.route(':userId/decks/:deckId')
    .get(pokemons.load_a_deck)
    .put(pokemons.update_a_deck)
    .delete(pokemons.delete_a_deck);

  app.route(':userId/decks/:deckId/cards')
    .get(pokemons.load_all_cards_from_deck)
    .post(pokemons.add_new_card_to_deck)
    .delete(pokemons.delete_a_card_from_deck);
};