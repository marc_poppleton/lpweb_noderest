'use strict';
var mongoose = require('mongoose');
require('mongoose-type-url');
var Schema = mongoose.Schema;


var CardSchema = new Schema({
  pokeapi_id: {
    type: String,
    required: 'ID from PokeAPI.co is required'
  },
  name: {
    type: String,
    required: 'Name of the card is required'
  },
  url: {
      type: mongoose.SchemaTypes.Url,
      required: 'Ref to URL from PokeAPI.co is mandatory'
  }, 
  Added_date: {
    type: Date,
    default: Date.now
  }
});

var DeckSchema = new Schema({
    name: {
        type: String
    },
    owner: {
      type: String,
      required: 'Deck owner is required'
    },
    cards:[{type: CardSchema}]
})

module.exports = mongoose.model('Cards', CardSchema);
module.exports = mongoose.model('Decks', DeckSchema);