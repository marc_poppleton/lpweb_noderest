'use strict';

var mongoose = require('mongoose'),
Deck = mongoose.model('Decks'),
Card = mongoose.model('Cards');

exports.load_all_decks = function(req, res) {
  Deck.find({}, function(err, deck) {
    if (err)
      res.send(err);
    res.json(deck);
  });
};

exports.load_all_decks_for_user = function(req, res) {
  Deck.find({owner: req.params.userId}, function(err, deck) {
    if (err)
      res.send(err);
    res.json(deck);
  });
};

exports.create_a_deck_for_user = function(req, res) {
  var new_deck = new Deck(req.body);
  new_deck.save(function(err, deck) {
    if (err)
      res.send(err);
    res.json(deck);
  });
};

exports.load_a_deck = function(req, res) {
  Deck.findById(req.params.deckId, function(err, deck) {
    if (err)
      res.send(err);
    res.json(deck);
  });
};

exports.update_a_deck = function(req, res) {
  Deck.findOneAndUpdate({_id: req.params.deckId}, req.body, {new: true}, function(err, deck) {
    if (err)
      res.send(err);
    res.json(deck);
  });
};

exports.delete_a_deck = function(req, res) {
  Deck.remove({_id: req.params.taskId}, function(err, deck) {
    if (err)
      res.send(err);
    res.json({ message: 'Deck successfully deleted' });
  });
};

exports.load_all_cards_from_deck = function(req,res) {
  Deck.findById(req.params.deckId, function(err,deck) {
    if (err)
      res.send(err);
    res.json(deck.cards);
  });
};

exports.add_new_card_to_deck = function(req, res) {
  var new_card = new Card(req.body);
  new_card.save(function(err, card) {
    if(err)
      req.send(err)
    Deck.findOneAndUpdate(
      {_id: req.params.deckId},
      {$push: {cards: new_card}},
      function(err, deck) {
        if (err)
          res.send(err)
        res.json(deck);
      }
    );
  })
};

exports.delete_a_card_from_deck = function(req, res) {
  Card.findById(req.params.cardId, function(err,card) {
    if (err)
      res.send(err);
    Deck.findOneAndUpdate(
      {_id: req.params.deckId},
      {$pop: {cards: card}},
      function(err, deck) {
        if (err)
          res.send(err)
        res.json(deck);
      }
    )
  })
};